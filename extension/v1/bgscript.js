// PENDING:
// 1.-rename all requests URL to api/call instead of api/clients
//** Este archivo se encarga de identificar los cambios en le storage de google
// y se manejan dos casos:
// 1.- cuando consumerPhone.oldevalue es falsy( que significa que es el primer nunmero
// que se captura desde que el storage estaba vacio, esto puede pasar al reiniciar
// la extension o borrar el storage porque llego a su maxima capacidad.
// 2.- cuando el consumerPhone.oldValue sea truty y newValue != oldValue
// esto significa que se capturo un numero diferente al que está guardado en el
// store de chrome(la extension hace un set al store de chrome que se asigna
// un valor a buttonClicked o disposition, si los datos no han cambiado entonces
// no se envia el request.

var instance_token;
chrome.storage.onChanged.addListener(function (changes, namespace) {
  var consumerPhone = changes['consumerPhone'];//valor elegido

  console.log(changes);
  if (consumerPhone) {
    console.log("****changes on store*****");
    //en la primera llamada 'consumerPhone.oldValue' no  estara definido pero queremos guardarlo tambien.
    if (!consumerPhone.oldValue) {
      var promise = new Promise(function (resolve) {
        chrome.storage.sync.get(function (result) {
          resolve(result);
        });
      });

      promise.then( data => {
        var url = 'http://54.67.51.82:1337/api/call/';
        fetch(url, {
          method: 'POST',
          body: JSON.stringify(data),
          headers:{
            'Content-Type': 'application/json'
          }
        }).then(response => console.log('Success:', JSON.stringify(response)))
        .catch(error => console.error('Error:', error));
      });
    }
    if (consumerPhone.oldValue != undefined && consumerPhone.newValue != consumerPhone.oldValue ) {
      //Cuando el valor elegido cambie, entonces hacemos el request al backend para guardar la información
      //Despues de hacer el request, borrar todo el contenido del storage sin perder 'consumerPhone.oldValue'
      //esto para no saturar el store de chrome

      var promise = new Promise(function (resolve) {
        chrome.storage.sync.get(function (result) {
          resolve(result);
        });
      });

      promise.then( data => {
        var url = 'http://54.67.51.82:1337/api/call/';
        fetch(url, {
          method: 'POST',
          body: JSON.stringify(data),
          headers:{
            'Content-Type': 'application/json'
          }
        }).then(response => console.log('Success:', JSON.stringify(response)))
        .catch(error => console.error('Error:', error));
      });
    }
 }
});

// solo se ejecuta al refrescar e instalar la extension la extension
//chrome.storage.sync.clear(function () {
 //console.log('storage cleared');
//});

chrome.runtime.onInstalled.addListener(function(details) {
  console.log('on installed');
  let reason = details.reason
  let isTokenSet = new Promise(function (resolve) {
    chrome.storage.sync.get('instance_token', function (result) {
      resolve(result);
    });
  });

  isTokenSet.then( currentToken => {
    if (reason == "update" && Object.keys(currentToken).length > 0) {
      // don't do anything because, it was only a refresh and we still have a token
      // This is necessary because we don't want to create  new token if we
      // already have one, this case will happend when the extension is restarted
      // this way we will use the same token that we had before the refresh
      return;
    }
  });

  if (reason == "install") {
    onInstall(reason);
  }
});

async function onInstall(reason) {
  await setToken();
  await createNewInstance(reason);
  // la declaración de setUninstallURL debe estar despues de que el token ha sido guardado en el store
  // de lo contrario la variable 'instance_token' estará vacía y cuando se desinstale la extensión
  // el query param será undefined
  chrome.runtime.setUninstallURL('http://localhost:8000/api/extension_instances/uninstall?token=' + instance_token);
  console.log('installation complete');
}

async function createNewInstance(reason) {
  var url = 'http://localhost:8000/api/extension_instances/';
  let response = await fetch(url, {
    method: 'POST',
    body: JSON.stringify({ token: instance_token, creation_reason: reason }),
    headers:{
      'Content-Type': 'application/json'
    }
  });
  if(!response.ok) {
    const message = `An error has occured: ${response.status}`;
    console.log(message);
  }
}

async function setToken() {
  let token = Math.random().toString(36).substring(2) + Math.random().toString(36).substring(2);
  return new Promise(resolve => {
    chrome.storage.sync.set({ instance_token: token }, function () {
      instance_token = token;
      resolve(token);
      console.log('set');
    })
  });
}
