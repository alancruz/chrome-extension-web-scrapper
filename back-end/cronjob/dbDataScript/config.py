#!/usr/bin/python
from configparser import ConfigParser
import os

def config():
    # form params for db connection
    db = {
        'host': os.environ.get('DB_HOST'),
        'database': os.environ.get('DB_NAME'),
        'user': os.environ.get('DB_USER'),
        'password': os.environ.get('DB_PASSWORD')
    }

    return db
