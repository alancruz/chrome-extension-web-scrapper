from django.db import models

class Call(models.Model):
    firstName = models.CharField(max_length=20)
    lastName = models.CharField(max_length=20)
    consumerPhone = models.CharField(max_length=10)
    consultantName = models.CharField(max_length=50, null=True)
    consultantPhone = models.CharField(max_length=10, null=True)
    disposition = models.CharField(max_length=100, null=True)
    state = models.CharField(max_length=50)
    zipCode = models.PositiveIntegerField()
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True)
    extension_instance = models.ForeignKey('ExtensionInstance', blank=True, null=True, default=None, on_delete=models.DO_NOTHING)

class ExtensionInstance(models.Model):
    token = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    status = models.BooleanField(default=True)
    creation_reason = models.CharField(max_length=50, default=None)

    def __str__(self):
        return self.token

class calls_daily_summary(models.Model):
    calls_count = models.IntegerField(default=list)
    states_count = models.JSONField(default=list)
    dispositions_count = models.JSONField(default=list)
    instances_count = models.IntegerField(default=list)
    summary_date = models.DateTimeField(null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
