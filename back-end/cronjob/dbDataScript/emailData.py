#!/usr/bin/env python
import sendgrid
from callsDataQueries import *
from sendgrid.helpers.mail import *
import os

def connect():
    try:
        # create row and get its ID
        last_row_id = getAllData()
        print("last_row_id: ", last_row_id)
    except Exception as error:
        print('An exception occurred while creating a new row in db: {}'.format(error))
        return

    try:
        url = os.environ.get('CHART_PAGE_URL') + str(last_row_id)
        print("generated url: ", url)
        email_content = '<body><p>To see the Daily Calls Summary please visit: <a href={}>Here<a/></p></body>'.format(url)
    except ValueError as error:
        print('An exception occurred while creating the url: {}'.format(error))
        return

    try:
        ## send email
        sg = sendgrid.SendGridAPIClient(api_key=os.environ.get('SENDGRID_API_KEY'))
        from_email = Email(os.environ.get('SENGRID_FROM_EMAIL'))
        to_email = To(os.environ.get('TO_EMAIL'))
        subject = "Daily Calls Summary"
        content = Content("text/html", email_content)
        mail = Mail(from_email, to_email, subject, content)


        ## Add BCC_EMAILS_STRING emails as CC
        if os.environ.get('BCC_EMAILS_STRING') and len(os.environ.get('BCC_EMAILS_STRING')) > 0:
            bcc_emails_list = os.environ.get('BCC_EMAILS_STRING').split(",")
            for email in bcc_emails_list:
                mail.personalizations[0].add_cc(Email(email))

        response = sg.client.mail.send.post(request_body=mail.get())
        print("Status code: ", response.status_code)
        print("Response body: ", response.body)
        print("Response headers: ", response.headers)
    except Exception as error:
        print('An exception occurred while sending the email: {}'.format(error))
        return

    return 'Email sent successfully'


if __name__ == '__main__':
    connect()
