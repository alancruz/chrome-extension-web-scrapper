import psycopg2
from psycopg2.extras import RealDictCursor
from config import config
import json
from datetime import datetime, timedelta


""" Connect to the PostgreSQL database server """


def getAllData():
    conn = None
    try:
        # read connection parameters
        params = config()

        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params)

        # create a cursor
        cur = conn.cursor(cursor_factory=RealDictCursor)

        # count yesterday's calls
        cur.execute("SELECT count(*) from back_end_call where created_at::date = (current_date - INTERVAL '1 day')::date;")
        calls_count = cur.fetchone()

        # count of instances working per day
        cur.execute("select count(distinct(extension_instance_id)) from back_end_call where created_at::date = (current_date - INTERVAL '1 day')::date and extension_instance_id is not null")
        instances_per_day = cur.fetchone()

        # count each disposition per day
        cur.execute("select disposition as column, count(*) from back_end_call where created_at::date = (current_date - INTERVAL '1 day')::date GROUP BY disposition")
        dispositions_json = json.loads(json.dumps(cur.fetchall()))

        # count of calls by state
        cur.execute("select state as column, count(*) from back_end_call where created_at::date = (current_date - INTERVAL '1 day')::date GROUP BY state")
        per_state_json = json.loads(json.dumps(cur.fetchall()))

        states = parseData(per_state_json)
        dispositions = parseData(dispositions_json)

        # insert new row into table
        yesterday_date = datetime.now() - timedelta(days=1)
        today_date = datetime.now()
        insert_query = """
        insert into back_end_calls_daily_summary (calls_count, states_count, dispositions_count, instances_count, summary_date, created_at)
        VALUES (%s, %s, %s, %s, %s, %s) RETURNING id
        """

        cur.execute(
            insert_query,
            (calls_count['count'], states, dispositions, instances_per_day['count'], yesterday_date, today_date)
        )

        id_of_new_row = json.loads(json.dumps(cur.fetchone()))['id']

        # close the communication with the PostgreSQL
        cur.close()

        return id_of_new_row
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.commit()
            conn.close()
            print('Database connection closed.')

def parseData(data):
    parsedData = {
        'labels': [],
        'data': []
    }

    for record in data:
        parsedData['labels'].append(record['column'])
        parsedData['data'].append(record['count'])

    return json.dumps(parsedData)
