### Bpo Utility
Run make setup to copy env.example to .env file.

Run `docker-compose up -d` to start project.

For development environment change the database name to `dev` in /API/settings.py file:

```python
    DATABASES = {
        'default': {
        'ENGINE': 'django.db.backends.postgresql',
            'NAME': 'dev',
            'USER': '***',
            'PASSWORD': '**',
            'HOST': 'db',
            'PORT': '5432',
        }
    }
```

# Cronjob
The cronjob service get its env vars from the root path. In Order tu run the cronjob service properly make sure to define this variables in your `.env` file:
- SENDGRID_API_KEY
- SENGRID_FROM_EMAIL
- SUMMARY_EMAIL_1
- SUMMARY_EMAIL_2
