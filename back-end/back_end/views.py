import os
import requests
import json
import csv
from .models import Call, ExtensionInstance, calls_daily_summary
from .serializers import CallSerializer, ExtensionInstanceSerializer
from rest_framework import viewsets
from .pagination import CustomPagination
from rest_framework.response import Response
from rest_framework.decorators import action
from django.http import HttpResponse, HttpResponseRedirect
from random import randrange
from .usStates import states
from .dispositions import dispositions
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect, render
import datetime
from django.core import serializers
import pdb
from django.contrib.auth.decorators import login_required


@login_required
def chartsPage(request, id):
    summary = calls_daily_summary.objects.get(id=id)
    serialized_obj = serializers.serialize('json', [summary, ])
    context = {}
    context['summary'] = json.loads(serialized_obj)[0]['fields']

    print(json.loads(serialized_obj)[0]['fields'])
    return render(request, 'charts/charts.html', context)


def loginPage(request):
    context = {}
    if 'next' in request.GET and len(request.GET['next']) > 0:
        context['login_redirect'] = request.GET['next']
    if request.method == 'POST':
        json_data = json.loads(request.body)
        username = json_data['username']
        password = json_data['password']

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            if 'next' in request.GET and len(request.GET['next']) > 0:
               return HttpResponseRedirect(context['login_redirect'])
            return redirect('call-report-builder')

    return render(request, 'login.html', context)


def logoutPage(request):
    logout(request)
    return redirect('login')


class CallViewSet(viewsets.ModelViewSet):
    queryset = Call.objects.all()
    serializer_class = CallSerializer

    def get_queryset(self):
        queryset = Call.objects.all()
        if self.request.query_params.get('startDate') == None:
            return queryset
        startDate = self.request.query_params.get('startDate', None)
        endDate = self.request.query_params.get('endDate', None)
        firstName = self.request.query_params.get('firstName', None)
        lastName = self.request.query_params.get('lastName', None)
        states = self.request.GET.getlist('selectedStates[]') or None
        dispositions = self.request.GET.getlist('selectedDispositions[]') or None
        consumerPhone = self.request.query_params.get('consumePhone', None)
        zipCode = self.request.query_params.get('zipCode', None)
        ordering = self.request.query_params.get('ordering', None)

        ## add one day to endDate because __Range won’t include items on the last day
        newEndDate = datetime.datetime.strptime(endDate, '%Y-%m-%d')
        endDatePlusOne = newEndDate + datetime.timedelta(days=1)
        queryset = queryset.filter(created_at__range=[startDate, endDatePlusOne])

        if firstName is not None:
            queryset = queryset.filter(firstName=firstName)
        if lastName is not None:
            queryset = queryset.filter(lastName=lastName)
        if states is not None:
            queryset = queryset.filter(state__in=states)
        if dispositions is not None:
            queryset = queryset.filter(disposition__in=dispositions)
        if consumerPhone is not None:
            queryset = queryset.filter(consumerPhone=consumerPhone)
        if zipCode is not None:
            queryset = queryset.filter(zipCode=zipCode)
        if ordering is not None:
            queryset = queryset.order_by(ordering)
        else:
            queryset = queryset.order_by('-created_at')

        return queryset

    @action(detail=False, methods=['get'], url_name='report-builder')
    def report_builder(self, request):
        if not request.user.is_authenticated:
            return redirect('login')
        context = {}
        extensionInstances = ExtensionInstance.objects.filter(status=True)
        context['extensionInstances'] = json.dumps(list(extensionInstances.values('token', 'status')))
        context["states"] = json.dumps(states)
        context["dispositions"] = dispositions

        return render(request, 'index.html', context)

    @action(detail=False, methods=['get'])
    def callsToCSV(self, request):
        queryset = self.get_queryset()
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment;filename="export.csv"'
        writer = csv.writer(response)
        writer.writerow([field.name for field in Call._meta.get_fields()])
        querysetValues = queryset.values_list(
            'id',
            'firstName',
            'lastName',
            'consumerPhone',
            'consultantName',
            'consultantPhone',
            'disposition',
            'state',
            'zipCode',
            'created_at',
            'updated_at',
            'extension_instance'
        )
        for call in querysetValues:
            writer.writerow(call)

        return response

    @action(detail=False, methods=['post'])
    def storeTransferData(self, request):
        agentNumber = randrange(1, 6)
        agent = os.environ.get("AGENT_EMAIL_" + str(agentNumber))
        password = os.environ.get("AGENTS_PASSWORD")
        transferId = request.data['transferId']
        consultantName = request.data['consultantName']
        consultantPhone = request.data['consultantPhone']
        instanceToken = request.data['instanceToken']
        instanceId = ExtensionInstance.objects.get(token=instanceToken)
        transferURL = str(os.environ.get("PARROT_API_URL")) + str(transferId)

        response = requests.post(os.environ.get("PARROT_BASE_URL") + 'login',
                                 json={
                                     'username': agent,
                                     'password': password
                                 },
                                 headers={
                                     "content-type": "application/json"
                                 })

        headers = {'Authorization': 'Bearer ' + response.json()['token']}
        transferData = requests.get(transferURL, headers=headers).json()

        callData = {
            'firstName': transferData['lead']['name'],
            'lastName': transferData['lead']['lastname'],
            'consumerPhone': transferData['lead']['homephone'],
            'disposition': transferData['disposition']['name'],
            'state': transferData['lead']['st'],
            'zipCode': transferData['lead']['zip'],
            'consultantName': consultantName,
            'consultantPhone': consultantPhone,
            'extension_instance': instanceId
        }

        call = Call.objects.create(**callData)
        return HttpResponse(status=200)


class ExtensionInstanceViewSet(viewsets.ModelViewSet):
    queryset = ExtensionInstance.objects.all()
    serializer_class = ExtensionInstanceSerializer
    pagination_class = CustomPagination

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.status = False
        instance.save()
        return Response(data="Token removed sucessfully")

    @action(detail=False, methods=['get'])
    def uninstall(self, request):
        instance = ExtensionInstance.objects.get(token=request.query_params['token'])
        instance.status = False
        instance.save()
        return HttpResponse("<html><body><p>Bpo Utility Uninstalled</p></body></html>")
