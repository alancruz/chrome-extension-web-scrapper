let transfer = null;
const TRANSFER_REGEX= /\d+/g;
window.setInterval( () => {
  let element = document.querySelector('div.panel-heading').innerHTML;
  let newTransfer = parseInt(element.match(TRANSFER_REGEX)[0]);
  if(newTransfer != transfer) {
    transfer = newTransfer;
    agentName = document.querySelector('#dropdown').innerText;
    agentPhone = document.querySelector('nav > div > div.navbar-right > div > button').innerText;
    chrome.storage.sync.set({consultantName: agentName});
    chrome.storage.sync.set({consultantPhone: agentPhone});
    chrome.storage.sync.set({transferId: transfer});
  }
}, 1000);