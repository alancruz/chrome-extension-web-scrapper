//const URL = 'http://54.67.51.82:1337/api/clients/';
// TOD : agregar extension_instance feature
const URL =  'http://54.67.51.82:1337/api/clients/storeTransferData/'
chrome.storage.onChanged.addListener(async function (changes, namespace) {
  if(changes["transferId"] && changes["transferId"].oldValue) {
    let getConsultantName = new Promise(function(resolve) {
      chrome.storage.sync.get(['consultantName'], function (result) {
        resolve(result);
      });
    });
    let getConsultantPhone = new Promise(function(resolve){
      chrome.storage.sync.get(['consultantPhone'], function (result) {
        resolve(result);
      });
    });
    let instanceToken = new Promise(function(resolve){
      chrome.storage.sync.get(['instance_token'], function (result) {
        resolve(result);
      });
    });
    Promise.all([getConsultantName, getConsultantPhone, instanceToken]).then(values => {
      let consultantName = values[0].consultantName.replace(/Hello/, "").trim();
      let data = {
        transferId: changes["transferId"].oldValue,
        consultantName: consultantName,
        consultantPhone: values[1].consultantPhone,
        instanceToken: values[2].instance_token
      }
      fetch(URL, {
        method: 'POST',
        body: JSON.stringify(data),
        headers:{
          'Content-Type': 'application/json'
        }
      }).then(response => console.log('Success:', JSON.stringify(response)))
      .catch(error => console.error('Error:', error));
    });
  }
});

// El siguiente código es para saber cuantas instancias existen corriendo el plugin
var instance_token;
chrome.runtime.onInstalled.addListener(function(details) {
  console.log('on installed');
  let reason = details.reason
  let isTokenSet = new Promise(function (resolve) {
    chrome.storage.sync.get('instance_token', function (result) {
      resolve(result);
    });
  });

  isTokenSet.then( currentToken => {
    if (reason == "update" && Object.keys(currentToken).length > 0) {
      // don't do anything because, it was only a refresh and we still have a token
      // This is necessary because we don't want to create  new token if we
      // already have one, this case will happend when the extension is restarted
      // this way we will use the same token that we had before the refresh
      return;
    }
  });

  if (reason == "install") {
    onInstall(reason);
  }
});

async function onInstall(reason) {
  await setToken();
  await createNewInstance(reason);
  // la declaración de setUninstallURL debe estar despues de que el token ha sido guardado en el store
  // de lo contrario la variable 'instance_token' estará vacía y cuando se desinstale la extensión
  // el query param será undefined
  chrome.runtime.setUninstallURL('http://54.67.51.82:1337/api/extension_instances/uninstall?token=' + instance_token);
  console.log('installation complete');
}

async function createNewInstance(reason) {
  var url = 'http://54.67.51.82:1337/api/extension_instances/';
  let response = await fetch(url, {
    method: 'POST',
    body: JSON.stringify({ token: instance_token, creation_reason: reason }),
    headers:{
      'Content-Type': 'application/json'
    }
  });
  if(!response.ok) {
    const message = `An error has occured: ${response.status}`;
    console.log(message);
  }
}

async function setToken() {
  let token = Math.random().toString(36).substring(2) + Math.random().toString(36).substring(2);
  return new Promise(resolve => {
    chrome.storage.sync.set({ instance_token: token }, function () {
      instance_token = token;
      resolve(token);
      console.log('set');
    })
  });
}
