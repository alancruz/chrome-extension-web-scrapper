// to test, put the commented lines in a setInterval to change the values on the html page
//var names = ['','alex', 'martha', 'pedro', 'eric', 'alan', 'memo', 'anayara', 'angel', 'pepe', 'cassandra']
//var phones = ['','6641234567', '9171234567', '9744525623', '9767548340', '9865232378', '9684527495', '6475960723', '5243649593', '909182736', '8762348976']
//var randomIndex = 0;
//var parentElement = document.querySelector('div .consumer-info .panel-body div');
//parentElement.querySelectorAll('div')[2].innerHTML = phones[Math.floor((Math.random() * 10) + 1)];
//parentElement.querySelectorAll('div')[0].innerHTML = names[Math.floor((Math.random() * 10) + 1)];
//element2.innerHTML = names[Math.floor((Math.random() * 10) + 1)];
var currentConsumerPhone = 0;
var buttonClicked = "";
let firstName = "";
let lastName = "";
let consumerPhone = 1;
let state = "";
let zipCode = "";
let consultantName = "";
let consultantPhone = 0;
let listenerAdded = false;

window.setInterval( () => {
  var parentElement = document.querySelector('div .consumer-info .panel-body div');
  if (parentElement) {
    firstName = parentElement.querySelectorAll('div')[0].innerHTML;
    lastName = parentElement.querySelectorAll('div')[1].innerHTML;
    consumerPhone = parentElement.querySelectorAll('div')[2].innerHTML;
    state = parentElement.querySelectorAll('div')[3].innerHTML;
    zipCode = parentElement.querySelectorAll('div')[4].innerHTML;
    consultantName = document.querySelector('a#dropdown.dropdown-toggle').innerHTML.replace(/Hello|\<.*/g, "").replace(/^\s|\s$/g, "");
    consultantPhone = document.querySelector('button.btn.btn-default').innerHTML.replace(/^\s|\s$/, "");
  }
}, 1000);

window.setInterval( () => {
    var isModalOpen = document.querySelector("div[role='dialog']");
    if(isModalOpen != null && consumerPhone != currentConsumerPhone && !listenerAdded){
      addListeners();
    }
    if(isModalOpen == null){
      listenerAdded = false;
    }
}, 100);

var sendData = function(){
    //Agregar todos los elementos que se necesitan observar y gurdarlos en el store
    chrome.storage.sync.set({firstName: firstName}, function () {
       var error = chrome.runtime.lastError;
       if (error.message && error.message == "This request exceeds the MAX_WRITE_OPERATIONS_PER_HOUR quota.") {
          chrome.storage.sync.clear(item => { console.log('storage clear on firstName')})
       }
    });
    chrome.storage.sync.set({lastName: lastName});
    chrome.storage.sync.set({state: state});
    chrome.storage.sync.set({zipCode: zipCode});
    chrome.storage.sync.set({consumerPhone: consumerPhone});
    chrome.storage.sync.set({consultantName: consultantName});
    chrome.storage.sync.set({consultantPhone: consultantPhone});
    chrome.storage.sync.set({buttonClicked: buttonClicked});
    currentConsumerPhone = consumerPhone;
}

var buttonBlock = document.getElementsByClassName("button-block");
var connectionBlock = document.getElementsByClassName("connection");

var wsFunction = function() {
  buttonClicked = this.textContent;
  sendData();
};

var addListeners = function(){
  for (var i = 0; i < buttonBlock.length; i++) {
      buttonBlock[i].addEventListener('click', wsFunction, false);
  }

  for (var i = 0; i < connectionBlock.length; i++) {
      connectionBlock[i].addEventListener('click', wsFunction, false);
  }
  listenerAdded = true;
}
