from rest_framework import serializers
from .models import Call, ExtensionInstance

class CallSerializer(serializers.ModelSerializer):
    class Meta:
        model = Call
        fields = ('id',
                  'firstName', 
                  'lastName', 
                  'consumerPhone', 
                  'consultantName', 
                  'consultantPhone', 
                  'disposition',
                  'state', 
                  'zipCode', 
                  'created_at',
                  'extension_instance')

class ExtensionInstanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExtensionInstance
        fields = ('id',
                'token',
                'creation_reason',
                'status',
                'created_at')
