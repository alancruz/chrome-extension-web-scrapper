**BPO Utility.**

This project is a web scrapper for a specific platform used in BPO solutions group.

it is divided in two parts the `chrome extension` and the `API`

***Extension***

This is a chrome extension that is used to get specific data from an specific in house platform.
Currently running `V2`.


***Back-end***

the back-end folder contains two services:
- The Django app.
- A service that runs a scheduled process(cronjob) for sending daily summary emails.

The folder's name is back-end because it started just as an API but later it grows to have views and a simple authentication system.

**For more information about setting up the environment for running the web app locally please see the back-end folder [README](https://gitlab.com/alancruz/chrome-extension-web-scrapper/-/blob/master/back-end/README.md) file**

**Web app technologies stack:**
- Vuejs
- Django
- PostgreSQL
- Docker
